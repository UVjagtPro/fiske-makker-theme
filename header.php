<!doctype html>

<html lang="da">

	<head>
		<?php wp_head() ?>


		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj8PSo0D5Vhr5uM_dJr4LyEJg2zS_cv94"></script>
		
		<?php if(is_front_page()){
			?><script async defer type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/main-map.js" charset="UTF-8"></script><?php 
		} ?>

		<?php if(is_singular('punkt')){
			?><script async defer type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/single-map.js" charset="UTF-8"></script><?php 
		} ?>

	</head>

	<body <?php body_class(); ?>>
			<header>
				<nav class="navbar navbar-dark bg-dark shadow-sm">
					<div class="container">	  
						<!-- Navbar content -->
						<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/img/fish.svg"><?php echo get_bloginfo( 'name' ); ?></a>
						
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>

						<?php if (is_user_logged_in()) {

							/*The WordPress Primary Menu*/
							wp_nav_menu(
								array(
									'theme_location'    => 'main-user-menu',
									'menu_class'        => 'navbar-nav ml-auto w-100 justify-content-end',
									'container_class'  	=> 'collapse navbar-collapse',
									'container_id'    	=> 'navbarNav'

								)
							);
						} else {
							
							/*The WordPress Primary Menu*/
							wp_nav_menu(
								array(
									'theme_location'    => 'main-visitor-menu',
									'menu_class'        => 'navbar-nav ml-auto w-100 justify-content-end',
									'container_class'  	=> 'collapse navbar-collapse',
									'container_id'    	=> 'navbarNav'
								)
							);
							
						} ?>
					</div>
				</nav>
			</header>

