<?php
	/*
	Template Name: Dine punkter
	*/

    get_header(); ?>
						

		<?php if (have_posts()) : 
			while (have_posts()) :the_post(); ?>
				<h2> <?php the_title(); ?></h2>
				<?php the_content(); ?>
			<?php endwhile; 
		endif; ?>


		<?php if (is_user_logged_in()) { ?>

			<table >

				<tr>
					<th>Titel</th>
					<th>Beskrivelse</th>
					<th>Handlinger</th>
				</tr>

				<?php $query = new WP_Query(array(
					'post_type' 		=> 	'punkt',
					'author' 			=>  $current_user->ID, 
					'posts_per_page' 	=>	'-1', 
					'post_status' 		=> 	array('publish', 'private') 
				) ); ?>

				<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

					<tr>
						<td><a href='<?php the_permalink(); ?>'><?php echo get_the_title(); ?></a></td>
						<td><?php the_excerpt(); ?></td>

						<?php $edit_post = add_query_arg('punkt-id', get_the_ID(), get_permalink(47 + $_POST['_wp_http_referer'])); ?>

						<td>
							<a href="<?php echo $edit_post; ?>">Rediger</a>
							<br>
							<?php if( !(get_post_status() == 'trash') ) : ?>

								<a onclick="return confirm('Er du sikker på at du vil slette følgende melding: <?php echo get_the_title() ?>?')"href="<?php echo get_delete_post_link( get_the_ID() ); ?>">Slet</a>

							<?php endif; ?>
							<br>
						</td>
					</tr>

				<?php endwhile; ?>

				<?php else : ?>

					<p>Du har ingen meldinger af fundet eller tabt udstyr. Meld udstyr tabt eller fundet <a href="<?php echo get_page_link(1865); ?>">her på kortet</a>.</p>

				<?php endif; ?>

			</table>

		<?php 
			} 
			else 
			{
		?>
			<p>Du skal være logget ind for at registrere tabt eller fundet udstyr. <a href="/wp-login.php">Log ind her på siden</a> eller <a href="/wp-login.php?action=register">registrer dig selv som bruger</a>.</p>

		<?php
			};
		?>
		
		<?php get_footer(); ?>
