<?php get_header();?>

<div class="container">

	<?php $args = array( 
		'post_type' => 'punkt'
	); ?>

	<?php $loop = new WP_Query( $args ); ?> 

	<?php if($loop->have_posts()) { ?>

		<div style="display: none;">
			<?php include "assets/php/load-map-polygons.php"; ?>

			<script type="text/javascript"> var polygons0 = <?php echo str_replace('"', '', json_encode($polygons0)); ?>;</script>
			<script type="text/javascript"> var polygons1 = <?php echo str_replace('"', '', json_encode($polygons1)); ?>;</script>
			<script type="text/javascript"> var polygons2 = <?php echo str_replace('"', '', json_encode($polygons2)); ?>;</script>
		</div>


	<?php } else {

		echo "Sorry! Nothing to display";
	} ?>

	<div class="row">
		<div id="map"></div>
	</div>

</div>

<?php get_footer(); ?>
