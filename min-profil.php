<?php

	/*
	Template Name: Min profil
	*/

?>

<?php get_header(); ?>


<?php logged_in_user_page(); ?>

<?php do_action( 'process_user_profile' ); ?>

<?php $user_id = get_current_user_id(); ?>
<?php $user_info = get_userdata( $user_id ); ?>	

<div class= "container">
	
	<div class="row">
		<div class="col-12" id="single-header">
			<h1> <?php the_title(); ?> </h1>
		</div>
	</div>


	<div class="row">
		<div class="col-12" id="single-content">
			<form role="form" action="" id="user_profile" method="POST">
				<?php wp_nonce_field('user_profile_nonce', 'user_profile_nonce_field'); ?>
				
				<span>Mine oplysninger:</span>

				<div class="form-group">
					<label for="first_name">Fornavn</label>
					<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Fornavn" value="<?php echo $user_info->first_name; ?>">
				</div>
				<div class="form-group">
					<label for="last_name">Efternavn</label>
					<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Efternavn" value="<?php echo $user_info->last_name; ?>">
				</div>
				<div class="form-group">
					<label for="email">E-mail addresse</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Indtast e-mail" value="<?php echo $user_info->user_email; ?>">
				</div>

				<div class="form-group">
					<label for="koen"><?php _e("Køn"); ?></label></br>
					<input type="radio" name="koen" value="Mand" <?php if('Mand'==esc_attr(get_the_author_meta('koen',$user_id ))) echo 'checked="checked"'; ?> class="radio" />&nbsp;&nbsp;Mand<br /> 
					
					<input type="radio" name="koen" value="Kvinde" <?php if('Kvinde'==esc_attr(get_the_author_meta('koen',$user_id ))) echo 'checked="checked"'; ?> class="radio" />&nbsp;&nbsp;Kvinde<br />

					<input type="radio" name="koen" value="Dette vil jeg ikke svare på" <?php if('Dette vil jeg ikke svare på'==esc_attr(get_the_author_meta('koen',$user->ID ))) echo 'checked="checked"'; ?> class="radio" />&nbsp;&nbsp;Dette vil jeg ikke svare på<br/><br/>
				</div>

				<!-- Get post meta value using the key from our save function in the second paramater. -->
				<?php $type_meta = get_user_meta( $user_id, 'type-meta-box', true); ?>

				<div class="form-group">
					<label for="type"><?php _e("Interesse"); ?></label></br>
					<input type="checkbox" name="type-meta-box[]" value="Stangfiskeri" <?php echo (in_array('Stangfiskeri', $type_meta)) ? 'checked="checked"' : ''; ?> />
					<label><?php _e("Stangfiskeri"); ?></label><br />

					<input type="checkbox" name="type-meta-box[]" value="Ruse- og garnfiskeri" <?php echo (in_array('Ruse- og garnfiskeri', $type_meta)) ? 'checked="checked"' : ''; ?> />
					<label><?php _e("Ruse- og garnfiskeri"); ?></label><br />

					<input type="checkbox" name="type-meta-box[]" value="Undervandsjagt" <?php echo (in_array('Undervandsjagt', $type_meta)) ? 'checked="checked"' : ''; ?> />
					<label><?php _e("Undervandsjagt"); ?></label><br />
				</div>

				<span>Opdater kodeord:</span>

				<div class="form-group">
					<label for="pass1">Kodeord</label>
					<input type="password" class="form-control" id="pass1" name="pass1" placeholder="Kodeord">
				</div>
				<div class="form-group">
					<label for="pass2">Gentag kodeord</label>
					<input type="password" class="form-control" id="pass2" name="pass2" placeholder="Kodeord">
				</div>

				<button type="submit" class="btn btn-dark">Gem</button>
			</form>
		</div>
	</div>
</div>

<?php get_footer(); ?>