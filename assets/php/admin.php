<?php

	/**
	 * Contains all functions that aid with customising the
	 * WordPress admin - The split of this code might not be 100% logical
	 * but for the purpose of the series it's nice to have the code in one file
	 */

	/**
	 * Style the wordpress login/register screens
	 * Uses conditonal loading of stylesheets
	 */

	function admin_css() {
	    if ( $_GET['action'] === 'register' ) {
	        wp_enqueue_style( 'register_css', get_template_directory_uri() . '/assets/css/register-form.css' );
	    } else {
	        wp_enqueue_style( 'login_css', get_template_directory_uri() . '/assets/css/custom-login.css' );
	    }
	}
	
	add_action('login_head', 'admin_css');

	/**
	* Change the link so the the replaced WP logo links to the site
	* http://codex.wordpress.org/Plugin_API/Filter_Reference/login_headerurl
	*/

	function the_url( $url ) {
	    return get_bloginfo( 'url' );
	}
	
	add_filter( 'login_headerurl', 'the_url' );

	/**
	* Change the link so the the replaced WP logo links to the site
	* http://codex.wordpress.org/Plugin_API/Filter_Reference/login_headerurl
	*/

	function register_intro_edit($message) {

	    if (strpos($message, 'Register') !== FALSE) {
	        
	        $register_intro = "Opret en bruger. Det er lige til!</br> Udfyld nedenstående formular.";
	        return '<p class="message register">' . $register_intro . '</p>';

	    } else {
	       
	        return $message;

	    }
	}

	add_action('login_message', 'register_intro_edit');

	/**
	* Adding the HTML to the existing registration form
	*/

	function register_form_edit() {

	    $terms = ( ! empty( $_POST['terms'] ) ) ? $_POST['terms'] : ''; ?>
	    
	    <p>
	        <label for="terms">
	            <input type="checkbox" name="terms" id="terms" class="input" value="agreed" <?php checked( $_POST['terms'], 'agreed', true ); ?> />
	            <?php _e( 'Jeg har læst og forstået vilkår og betingelser', 'sage' ) ?>
	        </label>
	    </p>
	    
	    <?php
	}

	add_action( 'register_form', 'register_form_edit' );

	function validate_registration( $errors, $sanitized_user_login, $user_email ) {

	    if ( empty( $_POST['terms'] ) ) {
	    
	        $errors->add( 'terms_error', __( '<strong>FEJL</strong>: Du skal acceptere vilkår og betingelserne.', 'sage' ) );
	    
	    }

	    return $errors;
	}
	add_filter( 'registration_errors', 'validate_registration', 10, 3 );

	/**
	* Process the additional fields.
	*
	* @param user_id
	*/

	function process_registration( $user_id ) {

		if ( ! empty( $_POST['terms'] ) ) {
			update_user_meta( $user_id, 'terms', trim( $_POST['terms'] ) );
		}
	}

	add_action( 'user_register', 'process_registration' );

	/**
	* Redirect user after successful login.
	*
	* @param string $redirect_to URL to redirect to.
	* @param string $request URL the user is coming from.
	* @param object $user Logged user's data.
	* @return string
	*/
	
	function redirect_on_login( $redirect_to, $request, $user ) {
		
		//is there a user to check?
		global $user;
		
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			
			//check for admins
			if ( in_array( 'administrator', $user->roles ) ) {
			
			    // redirect them to the default place
			    return $redirect_to;
			
			} else {
			
			    return home_url();
			
			}

		} else {

			return $redirect_to;

		}
	}
	
	add_filter( 'login_redirect', 'redirect_on_login', 10, 3 );

	function login_function() {
    
	    add_filter( 'gettext', 'username_change', 20, 3 );
	    
	    function username_change( $translated_text, $text, $domain ) 
	    {
	        if ($text === 'Username or Email Address') 
	        {
	            $translated_text = 'Brugernavn eller e-mail adresse';
	        }

	        if ($text === 'Username')
	        {
	            $translated_text = 'Brugernavn';
	        }

	        if ($text === 'Password')
	        {
	            $translated_text = 'Kodeord';
	        }

	        if ($text === 'Remember Me')
	        {
	            $translated_text = 'Husk mig';
	        }

	        if ($text === 'Log in')
	        {
	            $translated_text = 'Log ind';
	        }

	        if ($text === 'Register')
	        {
	            $translated_text = 'Registrer';
	        }

	        if ($text === 'Lost your password?')
	        {
	            $translated_text = 'Har du glemt dit kodeord?';
	        }

	        if ($text === 'Get New Password')
	        {
	            $translated_text = 'Gendan kodeord';
	        }

	        if ($text === 'I have read the terms and conditions')
	        {
	            $translated_text = 'Jeg har læst vilkårene og betingelserne';
	        }

	        if ($text === 'Registration confirmation will be emailed to you.')
	        {
	            $translated_text = 'Registreringsbekræftelsen vil blive sendt til dig via e-mail.';
	        }

	        if ($text === 'Please enter your username or email address. You will receive a link to create a new password via email.')
	        {
	            $translated_text = 'Indtast venligst dit brugernavn eller din emailadresse. Du vil modtage et link for at oprette en ny adgangskode via e-mail.';
	        }

	        return $translated_text;
	    }
    }

    add_action( 'login_head', 'login_function' );

?>