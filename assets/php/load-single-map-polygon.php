<?php 
	$singlepolygon = array();

	$zonedata = get_post_meta($post->ID,'fredningszone_data',false);

	$navn = 0;
	$kontaktsted = 0;
	$fredningsperiode = 0;
	$periodetype = 0;
	$lovgivningsgrundlag = 0;
	$bemaerkning = 0;
	$www = 0;
	$www2 = 0;						
	
	foreach ($zonedata as $data) {
		$navn = $data['navn'];
		$kontaktsted = $data['kontaktsted'];
		$fredningsperiode  = $data['fredningsperiode'];
		$periodetype = $data['periode_type'];
		$lovgivningsgrundlag = $data['lovgivningsgrundlag'];
		$bemaerkning = $data['bemaerkning'];
		$www = $data['www'];
		$www2 = $data['www2'];
	}

	$polypoints = get_post_meta($post->ID,'points',false);

	if((count($polypoints) > 0) && (!empty($polypoints))){ 

		$polygon = array();

		foreach($polypoints as $polypoint ){
			foreach($polypoint as $polypointdata ){

				$latlngelement = array();

				$latlng = explode(",", $polypointdata[3]);

				$latlngelement['lat'] = $latlng[0];
				$latlngelement['lng'] = $latlng[1];

				array_push($polygon,$latlngelement);

				unset($latlngelement);
			}
		}
	}

	// Push the array named $polygon into the array named $polygons
	array_push($singlepolygon,$polygon);

	// Empty out array named $point
	unset($polygon);


?>