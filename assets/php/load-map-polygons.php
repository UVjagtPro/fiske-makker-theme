<?php

	$loop = new WP_Query( 
		array( 
			'post_type' => 'punkt', 
			'posts_per_page' => -1
		) 
	);

	//$loopcount = 0;

	$polygons0 = array();
	$polygons1 = array();
	$polygons2 = array();

	if ($loop->have_posts()) { 

		?><div style="display: none;"><?php 

			$x = 0;
			
			while($loop->have_posts()) : $loop->the_post(); {

				$terms = get_the_terms($loop->ID, 'punktcat');

				foreach( $terms as $term ) {

					if ($term->term_id == 4) {

						$polypoints = get_post_meta($post->ID,'points',false);
						$zonedata = get_post_meta($post->ID,'fredningszone_data',false);

						$navn = 0;
						$kontaktsted = 0;
						$fredningsperiode = 0;
						$periodetype = 0;
						$lovgivningsgrundlag = 0;
						$bemaerkning = 0;
						$www = 0;
						$www2 = 0;						
						
						foreach ($zonedata as $data) {
							$navn = $data['navn'];
							$kontaktsted = $data['kontaktsted'];
							$fredningsperiode  = $data['fredningsperiode'];
							$periodetype = $data['periode_type'];
							$lovgivningsgrundlag = $data['lovgivningsgrundlag'];
							$bemaerkning = $data['bemaerkning'];
							$www = $data['www'];
							$www2 = $data['www2'];
						}

						if(($periodetype == 0) && (count($polypoints) > 0) && (!empty($polypoints))){

							?><div class="infobox" id="polygoninfo<?php echo $x; ?>">

								<h2><?php echo $navn; ?></h2>

								<ul>
									<li>Kategori: <?php echo $term->name; ?></li>
									<li>Fredningsperiode: <?php echo $fredningsperiode; ?></li>
									<li>Lovgivningsgrundlag: <?php echo $lovgivningsgrundlag; ?></li>
								</ul>

								<form action="<?php the_permalink(); ?>">
								    <button type="submit" class="btn btn-dark">Læs mere</button>
								</form>
							
							</div><?php
							
							$polygoninfostring = "document.getElementById('polygoninfo" . $x . "')";

							$polygon = array();

							foreach($polypoints as $polypoint ){
								
								foreach($polypoint as $polypointdata ){

									$latlngelement = array();

									$latlng = explode(",", $polypointdata[3]);

									$latlngelement['lat'] = $latlng[0];
									$latlngelement['lng'] = $latlng[1];
									$latlngelement['info'] = $polygoninfostring;


									array_push($polygon,$latlngelement);

									unset($latlngelement);
								}
							}

							// Push the array named $polygon into the array named $polygons
							array_push($polygons0,$polygon);

							// Empty out array named $point
							unset($polygon);

							$x++;

						} elseif (($periodetype == 1) && (count($polypoints) > 0) && (!empty($polypoints))) {
							
							?><div class="infobox" id="polygoninfo<?php echo $x; ?>">

								<h2><?php echo $navn; ?></h2>

								<ul>
									<li>Kategori: <?php echo $term->name; ?></li>
									<li>Fredningsperiode: <?php echo $fredningsperiode; ?></li>
									<li>Lovgivningsgrundlag: <?php echo $lovgivningsgrundlag; ?></li>
								</ul>

								<form action="<?php the_permalink(); ?>">
								    <button type="submit" class="btn btn-dark">Læs mere</button>
								</form>
							
							</div><?php
							
							$polygoninfostring = "document.getElementById('polygoninfo" . $x . "')";

							$polygon = array();

							foreach($polypoints as $polypoint ) {
								foreach($polypoint as $polypointdata ) {

									$latlngelement = array();

									$latlng = explode(",", $polypointdata[3]);

									$latlngelement['lat'] = $latlng[0];
									$latlngelement['lng'] = $latlng[1];
									$latlngelement['info'] = $polygoninfostring;


									array_push($polygon,$latlngelement);

									unset($latlngelement);

								}
							} 

							// Push the array named $polygon into the array named $polygons
							array_push($polygons1,$polygon);

							// Empty out array named $point
							unset($polygon);

							$x++;

						} elseif (($periodetype == 2) && (count($polypoints) > 0) && (!empty($polypoints))) {
							
							?><div class="infobox" id="polygoninfo<?php echo $x; ?>">

								<h2><?php echo $navn; ?></h2>

								<ul>
									<li>Kategori: <?php echo $term->name; ?></li>
									<li>Fredningsperiode: <?php echo $fredningsperiode; ?></li>
									<li>Lovgivningsgrundlag: <?php echo $lovgivningsgrundlag; ?></li>
								</ul>

								<form action="<?php the_permalink(); ?>">
								    <button type="submit" class="btn btn-dark">Læs mere</button>
								</form>
							
							</div><?php
							
							$polygoninfostring = "document.getElementById('polygoninfo" . $x . "')";

							$polygon = array();

							foreach($polypoints as $polypoint ) {
								foreach($polypoint as $polypointdata ) {

									$latlngelement = array();

									$latlng = explode(",", $polypointdata[3]);

									$latlngelement['lat'] = $latlng[0];
									$latlngelement['lng'] = $latlng[1];
									$latlngelement['info'] = $polygoninfostring;

									array_push($polygon,$latlngelement);

									unset($latlngelement);

								}
							} 

							// Push the array named $polygon into the array named $polygons
							array_push($polygons2,$polygon);

							// Empty out array named $point
							unset($polygon);

							$x++;

						} else {
							continue; 
						}
					}
				}
			} endwhile;

		?></div><?php
	}
		
?>