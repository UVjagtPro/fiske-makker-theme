var map; 

function initialize()
{
    var latlng = new google.maps.LatLng(55.95996, 11.76407);
    
    var myOptions = {
        zoom: 10,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        scaleControl: false,
        streetViewControl: false,
        /*streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },*/
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        fullscreenControl: false
    };
    
    var map = new google.maps.Map(document.getElementById("single-map"),
            myOptions);

    map.setOptions({ 
        minZoom: 2, 
        maxZoom: 20 
    });

    var polygonOptions;
    var polygon;

    if (singlepolygon.length > 0) {

        polygonOptions = {
            paths: singlepolygon, //'path' was not a valid option - using 'paths' array according to Google Maps API
            strokeWeight: "0",
            fillColor: '#ff0000', // Yellow
            fillOpacity: 0.5
        };

        polygon = new google.maps.Polygon(polygonOptions);

        polygon.setMap(map);
    }

    // Get center of polygon
    // Reference: http://jsfiddle.net/3eyqkpaf/
    var bounds = new google.maps.LatLngBounds();
    
    for (var i = 0; i < singlepolygon[0].length; i++) {
        
        console.log(singlepolygon[0][i].lat);
        console.log(singlepolygon[0][i].lng);

        var point = new google.maps.LatLng(
            singlepolygon[0][i].lat, singlepolygon[0][i].lng
        );

        bounds.extend(point);
    }
    
    map.fitBounds(bounds);

}

google.maps.event.addDomListener(window, 'load', initialize);




