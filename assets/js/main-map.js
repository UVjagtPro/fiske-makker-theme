var map;
const initialPosition = new google.maps.LatLng(55.95996, 11.76407); 

function initialize()
{
    
    var myOptions = {
        zoom: 8,
        center: initialPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        scaleControl: false,
        streetViewControl: false,
        /*streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },*/
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        fullscreenControl: false
    };
    
    var map = new google.maps.Map(document.getElementById("map"),
            myOptions);

    map.setOptions({ 
        minZoom: 2, 
        maxZoom: 20 
    });

    /*const customMarker = '/wp-content/themes/fiske-makker-theme/assets/img/markers/marker-3.svg';*/

    /*var locationMarker = {

        url: customMarker,
        anchor: new google.maps.Point(24,40),
        scaledSize: new google.maps.Size(48,40)
    }*/

    /*const marker = new google.maps.Marker({ 
        map, 
        position: initialPosition, 
        icon: locationMarker 
    });*/

    // Defining custom pulsing marker image
    const pulseMarkerImage = new google.maps.MarkerImage(
        '/wp-content/themes/fiske-makker-theme/assets/img/markers/pulse-marker.png',
        null, // size
        null, // origin
        new google.maps.Point( 8, 8 ), // anchor (move to center of marker)
        new google.maps.Size( 17, 17 ) // scaled size (required for Retina display icon)
    );

    // Create the pulsing marker
    const pulseMarker = new google.maps.Marker({
        flat: true,
        icon: pulseMarkerImage,
        map: map,
        optimized: false,
        position: initialPosition,
        title: 'My location',
        visible: true
    });

    // Get user's location
    // Reference: https://bagja.net/blog/track-user-location-google-maps.html
    if ('geolocation' in navigator) {
        
        navigator.geolocation.getCurrentPosition(
                position => {
                    //console.log(`Lat: ${position.coords.latitude} Lng: ${position.coords.longitude}`);

                    // Set marker's position.
                    pulseMarker.setPosition({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    });

                    // Center map to user's position.
                    map.panTo({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }
                );
            },
            err => alert(`Error (${err.code}): ${getPositionErrorMessage(err.code)}`)
        );
    } else {

        alert('Geolokation er ikke understøttet af din browser.');

    }



    // Location button
    addYourLocationButton(map, initialPosition);



    
    var polygonOptions0;
    var polygon0;

    // Load polygons and display them on map
    for (let i = 0; i < polygons0.length; i++) {
        if (polygons0[i].length > 0) {
            
            //console.log(polygons0[i][0].info);

            polygonOptions0 = {
                paths: polygons0[i], //'path' was not a valid option - using 'paths' array according to Google Maps API
                strokeWeight: "0",
                fillColor: '#ff0000', // Yellow
                fillOpacity: 0.5
            };

            polygon0 = new google.maps.Polygon(polygonOptions0);

            polygon0.setMap(map);

            polygon0.addListener('click', (function showPolygonInfoWindow(event) {
        
                // Since this polygon has only one path, we can call getPath() to return the
                let vertices = this.getPath();
                let contentString = polygons0[i][0].info;

                // Replace the info window's content and position.
                infoWindow.setContent(contentString);
                infoWindow.setPosition(event.latLng);

                infoWindow.open(map);
            }));

            infoWindow = new google.maps.InfoWindow;
        }
    }
 
    var polygonOptions1;
    var polygon1;

    // Load polygons and display them on map
    for (let i = 0; i < polygons1.length; i++) {
        if (polygons1[i].length > 0) {

            //console.log(polygons1[i][0].info);        

            polygonOptions1 = {
                paths: polygons1[i], //'path' was not a valid option - using 'paths' array according to Google Maps API
                strokeWeight: "0",
                fillColor: '#ff0000', // Red
                fillOpacity: 0.5
            };
            polygon1 = new google.maps.Polygon(polygonOptions1);

            //map needs to be defined somewhere outside of this loop
            //I'll assume you already have that.
            polygon1.setMap(map);

            polygon1.addListener('click', (function showPolygonInfoWindow(event) {
        
                // Since this polygon has only one path, we can call getPath() to return the
                let vertices = this.getPath();
                let contentString = polygons1[i][0].info;

                // Replace the info window's content and position.
                infoWindow.setContent(contentString);
                infoWindow.setPosition(event.latLng);

                infoWindow.open(map);
            }));

            infoWindow = new google.maps.InfoWindow;
        }  
    }
    
    var polygonOptions2;
    var polygon2;

    // Load polygons and display them on map
    for (let i = 0; i < polygons2.length; i++) {
        if (polygons2[i].length > 0) {
            
            //console.log(polygons2[i][0].info);

            polygonOptions2 = {
                paths: polygons2[i], //'path' was not a valid option - using 'paths' array according to Google Maps API
                strokeWeight: "0",
                fillColor: '#ff0000', // Blue
                fillOpacity: 0.5
            };
            polygon2 = new google.maps.Polygon(polygonOptions2);

            //map needs to be defined somewhere outside of this loop
            //I'll assume you already have that.
            polygon2.setMap(map); 

            polygon2.addListener('click', (function showPolygonInfoWindow(event) {
        
                // Since this polygon has only one path, we can call getPath() to return the
                let vertices = this.getPath();
                let contentString = polygons2[i][0].info;

                // Replace the info window's content and position.
                infoWindow.setContent(contentString);
                infoWindow.setPosition(event.latLng);

                infoWindow.open(map);
            }));

            infoWindow = new google.maps.InfoWindow;
        }
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Fejl: Geolokation servicen fejlede.' :
                          'Fejl: Din browser understøtter ikke geolokation.');
    infoWindow.open(map);
}


// Reference: https://jsfiddle.net/ogsvzacz/6/
function addYourLocationButton (map, pulseMarker) 
{
    var controlDiv = document.createElement('div');

    var firstChild = document.createElement('button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginRight = '10px';
    firstChild.style.marginBottom = '20px';
    firstChild.style.padding = '0';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0 0';
    secondChild.style.backgroundRepeat = 'no-repeat';
    firstChild.appendChild(secondChild);

    google.maps.event.addListener(map, 'center_changed', function () {
        secondChild.style['background-position'] = '0 0';
    });

    firstChild.addEventListener('click', function () {
        var imgX = '0',
            animationInterval = setInterval(function () {
                imgX = imgX === '-18' ? '0' : '-18';
                secondChild.style['background-position'] = imgX+'px 0';
            }, 500);

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(latlng);
                clearInterval(animationInterval);
                secondChild.style['background-position'] = '-144px 0';
            });
        } else {
            clearInterval(animationInterval);
            secondChild.style['background-position'] = '0 0';
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
}

google.maps.event.addDomListener(window, 'load', initialize);



