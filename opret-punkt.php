<?php
	/*
	Template Name: Opret punkt
	*/

	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('custom-js', get_template_directory_uri().'/assets/js/custom-js.js');
	wp_enqueue_style('jquery-ui-custom', get_template_directory_uri().'/assets/css/jquery-ui-custom.css');

	$postTitleError = '';
	$postContentError = '';
	$postCatError = '';

	if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) 
	{

		if(trim($_POST['postTitle']) == '') 
		{
			$postTitleError = '* Titel mangler at blive udfyldt';
			$hasError = true;
		} 
		else 
		{
			$postTitle = trim($_POST['postTitle']);
		}

		if (trim($_POST['postContent']) == '') 
		{ 
	    	
	    	$postContentError = '* Beskrivelse mangler at blive udfyldt';
			$hasError = true; 
		} 
		else 
		{ 
		    $postTitle = trim($_POST['postContent']);
		}

		if ($_POST['cat'] == '') 
		{ 
	    	$postCatError = '* Kategori mangler at blive valgt';
			$hasError = true; 
		} 
		else 
		{ 
		    $postTitle = $_POST['cat'];
		}

		$post_information = array(
			//'post_mime_type' 	=> 	$wp_filetype['type'],
			'post_title' 		=> 	esc_attr(strip_tags($_POST['postTitle'])),
			'post_content' 		=> 	esc_attr(strip_tags($_POST['postContent'])),
			'post_type' 		=> 	'punkt',
			'post_status' 		=> 	'publish'
		);

		$post_id = wp_insert_post($post_information);

		if($_FILES) 
		{
		    foreach ($_FILES as $file => $array) 
		    {
			    //$newupload = insert_attachment($file,$post_id);
			    // $newupload returns the attachment id of the file that
			    // was just uploaded. Do whatever you want with that now.
		    }
		}

		if($post_id)
		{
			// Update Custom Meta
			update_post_meta($post_id, '_location', esc_attr(strip_tags($_POST['customMetaOne'])));
			//update_post_meta($post_id, 'custom_date', esc_attr(strip_tags($_POST['customMetaTwo'])));

			wp_set_post_terms($post_id, $_POST['cat'], 'punktcat', false );

			wp_redirect(get_page_link(5));
			exit;
		}
	}
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) :the_post(); ?>
		<h2> <?php the_title(); ?> </h2>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
			

	<div style="display: none;">
		<?php	
			$queryURL = parse_url( html_entity_decode( esc_url( add_query_arg( $arr_params ) ) ) );
			parse_str( $queryURL['query'], $getVar );
			$latitude = $getVar['lat'];
			$longitude = $getVar['lng'];
			
			echo '<p>Latitude: ' . $latitude . '</br> Longitude: ' . $longitude . '</p>';
			echo '<p>Latitude og longitude: ' . $latitude . ',' . $longitude . '</p>';
		?>
	</div>

	<?php
	if ( is_user_logged_in() ) 
	{ ?>

	
		<form action="" id="primaryPostForm" method="POST" enctype="multipart/form-data">
			<fieldset>

				<label for="postTitle"><?php _e('Titel:', 'framework') ?></label>

				<br>

				<input type="text" name="postTitle" class="melding-field" id="postTitle" value="<?php if(isset($_POST['postTitle'])) echo $_POST['postTitle']; ?>" class="required"/>

			</fieldset>

			<?php if($postTitleError != '') { ?>
				<span class="error"><?php echo $postTitleError; ?></span>
				<div class="clearfix"></div>
			<?php } ?>

			<br>

			<fieldset>
				<label for="postKategori"><?php _e('Kategori:', 'framework') ?></label>
				<br>
			 	<?php $select_cats = wp_dropdown_categories(array('echo' => 0, 'taxonomy' => 'punktcat', 'hide_empty' => 0, 'show_option_none' => '&mdash;'));
                $select_cats = str_replace( "name='cat'", "name='cat[]' id='cat'", $select_cats );
                echo $select_cats;?>
			</fieldset>
			
			<?php if($postCatError != '') { ?>
				<span class="error"><?php echo $postCatError; ?></span>
				<div class="clearfix"></div>
			<?php } ?>

			<br>

			<fieldset class="images">
				
				<label for="postKategori"><?php _e('Billede:', 'framework') ?></label>
				
				<br>
				
				<input type="file" name="bottle_front" id="bottle_front">

			</fieldset>

			<br>

			<fieldset>

				<label for="customMetaTwo"><?php _e('Hvornår er udstyret tabt eller fundet udstyr:', 'framework') ?></label>

				<br>

				<input type="text" class="datepicker" name="custom_date" id="custom_date" value="<?php if(isset($_POST['customMetaTwo'])) echo $_POST['customMetaTwo'];?>" />

			</fieldset>

			<br>

			<fieldset>
			
				<label for="postContent"><?php _e('Beskrivelse:', 'framework') ?></label>

				<br>

				<textarea name="postContent" class="melding-field" id="postContent"><?php 
						if(isset($_POST['postContent'])) 
						{ 
							if(function_exists('stripslashes')) 
							{ 
								echo stripslashes($_POST['postContent']); 
							} else 
							{ 
								echo $_POST['postContent']; 
							} 
						} 
					?></textarea>

			</fieldset>

			<?php if($postContentError != '') { ?>
				<span class="error"><?php echo $postContentError; ?></span>
				<div class="clearfix"></div>
			<?php } ?>

			</br>

			<fieldset>

			    <label for="customMetaOne"><?php _e('Breddegrad og længdegrad:', 'framework') ?></label>
			 
			    <input type="text" name="customMetaOne" class="melding-field" id="customMetaOne" value="<?php echo $latitude . ','. $longitude; ?>" class="required" style="width:100%; background-color:rgb(200,200,200);" readonly/>
			 
			</fieldset>
			</br>
			<!--<div class="g-recaptcha" data-sitekey="6LdUbiITAAAAAGsHfq1nJPHgtJ42Qns6JAYk9DiN"></div>
			</br>-->

			<fieldset>
	
				<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>

				<input type="hidden" name="submitted" id="submitted" value="true" />
				<button type="submit" class="btn btn-default melding-button melding-field" id="frontpage-article-button">
					<?php _e('Tilføj melding', 'framework') ?>
				</button>
			</fieldset>
		</form>
			

	<?php } else { ?>

			<p>Du skal være logget ind for at registrere et punkt. <a href="/wp-login.php">Log ind her på siden</a> eller <a href="/wp-login.php?action=register">registrer dig selv som bruger</a>.</p>

	<?php
		};
	?>

	<script async type="text/javascript">
		jQuery(function() 
		{
			jQuery(".datepicker").datepicker();
		});
	</script>

<?php get_footer(); ?>
