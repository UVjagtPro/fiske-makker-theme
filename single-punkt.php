<?php get_header(); ?>

<?php global $current_user; ?>
<?php wp_get_current_user(); ?>

<div class="container">
	<?php while(have_posts()) : the_post(); ?>

			<!--####################################################################################################
			########################################################################################################
			############################### Fetching page data from server #########################################
			########################################################################################################
			#####################################################################################################-->

			<?php include "assets/php/load-single-map-polygon.php"; ?>

			<script type="text/javascript"> var singlepolygon = <?php echo str_replace('"', '', json_encode($singlepolygon)); ?>;</script>

			<!--####################################################################################################
			########################################################################################################
			############################### Page content ###########################################################
			########################################################################################################
			#####################################################################################################-->

			<div class="row">
				<div class="col-12" id="single-header">
					<h1><?php echo $navn; ?></h1>
				</div>
			</div>

			<div class="row">
				<div class="col-12" id="single-content">
					<ul class="list-group">
						<li class="list-group-item">Kontaktsted: <?php echo $kontaktsted; ?></li>
						<li class="list-group-item">Lovgivningsgrundlag: <?php echo $lovgivningsgrundlag; ?></li>
						<li class="list-group-item">Fredningsperiode: <?php echo $fredningsperiode; ?></li>
						<li class="list-group-item">Bemærkning: <?php echo $bemaerkning; ?></li>
						<li class="list-group-item">WWW: <a target="_blank" href="<?php echo $www; ?>">Læs mere www.retsinformation.dk</a></li>
						<li class="list-group-item">WWW2: <a target="_blank" href="<?php echo $www2; ?>"><?php echo $www2; ?></a></li>
						<li class="list-group-item">Kategori: <?php  $terms = get_the_terms($post->ID, 'punktcat'); ?>
						<?php foreach( $terms as $term ) {
							echo $term->name;
						} ?>
						</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div id="map-container">
					<div id="single-map"></div>
				</div>
			</div>

			<!--####################################################################################################
			########################################################################################################
			############################### Page navigation for single #############################################
			########################################################################################################
			#####################################################################################################-->

			</br>

			<div class="row">
				<div class="single-navigation">
					<?php if (is_user_logged_in() && $current_user->ID == $post->post_author) {?>
						
						<?php $edit_post = add_query_arg('punkt-id', get_the_ID(), get_permalink(2 + $_POST['_wp_http_referer'])); ?>

						<nav class="nav nav-pills flex-column flex-sm-row">
							<a class="flex-sm-fill text-sm-center nav-link btn-dark" href="<?php echo get_home_url(); ?>">Tilbage til kortet</a>
							<a class="flex-sm-fill text-sm-center nav-link btn-dark" href="<?php echo $edit_post; ?>">Rediger melding</a>
							<?php if( !(get_post_status() == 'trash') ) : ?>
								<a class="flex-sm-fill text-sm-center nav-link btn-dark" onclick="return confirm('Er du sikker på at du vil slette følgende melding: <?php echo get_the_title() ?>?')" href="<?php echo get_delete_post_link( get_the_ID() ); ?>">Slet melding</a>
							<?php endif; ?>
						</nav>

					<?php } else { ?>
						<nav class="nav nav-pills flex-column flex-sm-row">
							<a class="flex-sm-fill text-sm-center nav-link btn-dark" href="<?php echo get_home_url(); ?>">Tilbage til kortet</a>
						</nav>
					<?php } ?>
				</div>
			</div>

			</br>

	<?php endwhile; wp_reset_query(); ?>	
</div>

<?php get_footer(); ?>