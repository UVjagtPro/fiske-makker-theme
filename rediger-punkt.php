<?php
	/*
	Template Name: Rediger punkt
	*/
	
	$query = new WP_Query(
		array(
			'post_type' 		=> 'punkt',
			'posts_per_page'	=>'-1', 
			'post_status' 		=> array('publish', 'private') 
			) 
		);

	$queryURL = parse_url( html_entity_decode(esc_url( add_query_arg($arr_params))));
	parse_str( $queryURL['query'], $getVar );
	$observationID = $getVar['punkt-id'];

	if ($query->have_posts()) : {

		while ($query->have_posts()) : $query->the_post();
	
			if(isset($observationID)) 
			{
				if($observationID == $post->ID)
				{
					$current_post = $post->ID;

					$punktid = get_the_ID();

					$title = get_the_title();
					$content = get_the_content();

					// $selected_date = get_post_meta($current_post, 'custom_date', true);

					$terms = get_the_terms($post->ID, 'punktcat'); 

					foreach( $terms as $term ) 
					{
						$showntermid = $term->term_id;
						/*$showntermname = $term->term_id;*/
					}

					$day = get_post_meta( $post->ID, '_fangst_day', true );
					$month = get_post_meta( $post->ID, '_fangst_month', true );
					$year = get_post_meta( $post->ID, '_fangst_year', true );
					$minute = get_post_meta( $post->ID, '_fangst_minute', true );
					$hour = get_post_meta( $post->ID, '_fangst_hour', true );

					//$date = get_the_terms($post->ID, '_fangst_year');

	       			/*$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
					$thumb_url_array = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail-size', true);
					$thumb_url = $thumb_url_array[0];*/
				}
			}

		endwhile; 
	} else : {
		echo "Sorry! No data to display";
	}
	endif;

	wp_reset_query();

	global $current_post;

	$postTitleError = '';

	if(isset($_POST['submitted']) && isset($_POST['post_nonce_field']) && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce')) : {
		
		if(trim($_POST['postTitle']) == '') {
			
			$postTitleError = 'Please enter a title.';
			$hasError = true;

		} else {
			
			$postTitle = trim($_POST['postTitle']);

		}

		$post_information = array(
			'ID' 				=> 	$current_post,
			'post_mime_type' 	=> 	$wp_filetype['type'],
			'post_title' 		=> 	esc_attr(strip_tags($_POST['postTitle'])),
			'post_content' 		=> 	esc_attr(strip_tags($_POST['postContent'])),
			'post_type' 		=> 	'punkt',
			'post_status' 		=> 	'publish'
		);

		$post_id = wp_update_post($post_information);

		if($_FILES) 
		{
		    foreach ($_FILES as $file => $array) 
		    {
			    $newupload = insert_attachment($file, $post_id);
			    // $newupload returns the attachment id of the file that
			    // was just uploaded. Do whatever you want with that now.
		    }
		}

		if($post_id)
		{
			// update_post_meta($post_id, 'custom_date', esc_attr(strip_tags($_POST['customMetaTwo'])));

			wp_set_post_terms($post_id, $_POST['cat'], 'punktcat', false );

			wp_redirect(get_page_link($punktid));

			exit;
		}
	} endif; ?> 

	<?php get_header(); ?>
						
	<?php if (have_posts()) : {

		while (have_posts()) :the_post(); ?>

			<h1><?php the_title(); ?>: <?php echo $title ?></h1>
		
		<?php endwhile;
	} else : {
		echo "Sorry! No data to display";
	} endif; ?>
				

	<?php if ( is_user_logged_in()) : { 
		
		$queryURL = parse_url( html_entity_decode( esc_url( add_query_arg( $arr_params ) ) ) );
		parse_str( $queryURL['query'], $getVar );
		$observationID = $getVar['punkt-id'];
			
		/* echo '<p>Observation post ID: ' . $observationID . '</p>'; */
		?>

		<form action="" id="primaryPostForm" method="POST" enctype="multipart/form-data">

			<fieldset>

				<label for="postTitle"><?php _e('Titel:', 'framework') ?></label>

				<br>

				<input type="text" name="postTitle" class="melding-field" id="postTitle" value="<?php echo $title; ?>" class="required" />

			</fieldset>

			<?php if($postTitleError != '') { ?>
				<span class="error"><?php echo $postTitleError; ?></span>
				<div class="clearfix"></div>
			<?php } ?>

			<br>

			<fieldset>
				<label for="postKategori"><?php _e('Kategori:', 'framework') ?></label>
				<br>
			 	<?php 
			 		$select_cats = wp_dropdown_categories(array('echo' => 0, 'taxonomy' => 'punktcat', 'hide_empty' => 0, 'selected' => $showntermid, 'show_option_none' => '&mdash;'));
                	
                	$select_cats = str_replace( "name='cat'", "name='cat[]' id='cat'", $select_cats );
                
                	echo $select_cats;
            	?>
			</fieldset>

			<br>

			<fieldset>

				<label for="customMetaDate"><?php _e('Dato:', 'framework') ?></label>

				<br>

				<input type="text" name="day" id="day" value="<?php echo $day; ?>" />
				<input type="text" name="month" id="month" value="<?php echo $month; ?>" />
				<input type="text" name="year" id="year" value="<?php echo $year; ?>" />

			</fieldset>

			<br>

			<fieldset>

				<label for="customMetaDate"><?php _e('Tidspunkt:', 'framework') ?></label>

				<br>

				<input type="text" name="hour" id="hour" value="<?php echo $hour; ?>" /> <span>:</span>
				<input type="text" name="minute" id="minute" value="<?php echo $minute; ?>" />

			</fieldset>

			<br>

			<fieldset>
		
				<label for="postContent"><?php _e('Beskrivelse:', 'framework') ?></label>

				<br>

				<textarea name="postContent" class="melding-field" id="postContent"><?php echo $content; ?></textarea>

			</fieldset>

			<br>

			<fieldset>

				<?php wp_nonce_field('post_nonce', 'post_nonce_field'); ?>

				<input type="hidden" name="submitted" id="submitted" value="true" />
				<button type="submit" class="btn btn-default melding-button melding-field" id="frontpage-article-button">
					<?php _e('Opdater melding', 'framework') ?>
				</button>
			</fieldset>
		</form>

		<form class="btn btn-default melding-button melding-field melding-edit-cancel-form" id="frontpage-article-button" action="<?php echo get_page_link($punktid); ?>">
			<fieldset>		    
		    	<input type="submit" class="melding-edit-cancel" value="Fortryd">
		    </fieldset>
		</form>
		

	<?php } else : { ?>

		<p>Du skal være logget ind for at redigere et punkt. <a href="/wp-login.php">Log ind her på siden</a> eller <a href="/wp-login.php?action=register">registrer dig som bruger</a>.</p>
	
	<?php } endif; ?> 

<?php get_footer(); ?>
